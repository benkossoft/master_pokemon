package PokemonDB;

import java.io.IOException;
import java.util.LinkedList;

import javax.lang.model.element.Element;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import DatabaseSQL.SQL;

public class Collector {
	
	public static void main(String[] args) throws InterruptedException {
		
		LinkedList<Pokemon> pokedex = new LinkedList<Pokemon>();
		LinkedList<String> types = new LinkedList<String>();
		String pokemonName = null;
		String pokemonId = null;
		PokemonType type1 = null;
		PokemonType type2 = null;
		String website_image = null;
		int hp = 0;
		int attack = 0;
		int defense = 0;
		int specialAttack = 0;
		int specialDefense = 0;
		int speed = 0;
		Document document2 = null;

		try {
			Document doc = Jsoup.connect("http://pokemondb.net/pokedex/all").get();
			org.jsoup.select.Elements links = doc.select("tbody > tr");
			
			for(org.jsoup.nodes.Element element: links){
				if(element.child(1).children().size() < 2){
					pokemonId = element.child(0).ownText();
					pokemonName = element.child(1).child(0).ownText();
					String type1_str = element.child(2).child(0).ownText();
					if(!types.contains(type1_str)){
						types.add(type1_str);
					}
					type1 = returnType(type1_str);
					if(element.child(2).children().size() > 2){
						String type2_str = element.child(2).child(2).ownText();
						type2 = returnType(type2_str);
						if(!types.contains(type2_str)){
							types.add(type2_str);
						}
					}
						
					hp = Integer.parseInt(element.child(4).ownText());
					attack = Integer.parseInt(element.child(5).ownText());
					defense = Integer.parseInt(element.child(6).ownText());
					specialAttack = Integer.parseInt(element.child(7).ownText());
					specialDefense = Integer.parseInt(element.child(8).ownText());
					speed = Integer.parseInt(element.child(9).ownText());
					website_image = "http://img.pokemondb.net/artwork/"+pokemonName.toLowerCase()+".jpg";
					
					pokedex.add(new Pokemon(pokemonName, pokemonId, type1, type2, hp, attack, defense, specialAttack,
						    specialDefense, speed,
							website_image));
					
					type2 = null;
				}								
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
//		for(int i = 0; i < 151; i++){
//			Thread.sleep(300);
//			//SECOND METHOD BUT SITES TEMPORALILY UNAVALIABLE
//			try {
//				Document document = Jsoup.connect("http://pokemondb.net/pokedex/"+
//			pokedex.get(i).getName().toLowerCase()).get();
//				//Document document = Jsoup.connect("http://pokemondb.net/pokedex/").get();
//				org.jsoup.select.Elements links = document.select("div[class=col desk-span-9 lap-span-12] > div");
//				if(!links.isEmpty()){
//					for(org.jsoup.nodes.Element e: links){
//						if(e.childNodeSize() == 7){
//							//2 stage
//							//first stage
//							String firstStage = e.child(0).child(4).ownText();
//							System.out.print(firstStage + " >> ");
//							int first = e.child(1).toString().indexOf("(");
//							int second = e.child(1).toString().indexOf(")");
//							String level = e.child(1).toString().substring(first, second+1);
//							System.out.print(level + " >> ");
//							//second stage
//							String secondStage = e.child(2).child(4).ownText();
//							System.out.println(secondStage);
//						}
//						else if(e.childNodeSize() == 11){
//							//3stage
//							//first stage
//							String firstStage = e.child(0).child(4).ownText();
//							System.out.print(firstStage + " >> ");
//							int first = e.child(1).toString().indexOf("(");
//							int second = e.child(1).toString().indexOf(")");
//							String level = e.child(1).toString().substring(first, second+1);
//							System.out.print(level + " >> ");
//							//second stage
//							String secondStage = e.child(2).child(4).ownText();
//							System.out.print(secondStage + " >> ");
//							int first2 = e.child(3).toString().indexOf("(");
//							int second2 = e.child(3).toString().indexOf(")");
//							String level2 = e.child(3).toString().substring(first2, second2+1);
//							System.out.print(level2 + " >> ");
//							//third stage					
//							String thirdStage = e.child(4).child(4).ownText();
//							System.out.println(thirdStage);
//						}
//					}
//				}
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//		}
		
		
		
		//Kanto print;
//		for(int i = 0; i < 151; i++){
//			System.out.println(pokedex.get(i));
//		}
		
		//DATABASE PLAN
		DatabaseSQL.SQL sql = new SQL();
		sql.connectToDB("pokemon_world");
		for(String str : types){
			sql.insertIntoTypes(str);
		}
		
//		sql.insertIntoRegion("Kanto", 8);
//		sql.insertIntoRegion("Orange Island", 5);
//		sql.insertIntoRegion("Johto", 8);
//		sql.insertIntoRegion("Hoenn", 8);
//		sql.insertIntoRegion("Sinnoh", 8);
//		sql.insertIntoRegion("Unova", 8);
//		sql.insertIntoRegion("Kalos", 8);
		
		sql.insertIntoCanBeat("Fire", "Grass");
		sql.insertIntoCanBeat("Fire", "Ice");
		sql.insertIntoCanBeat("Fire", "Bug");
		sql.insertIntoCanBeat("Fire", "Steel");
		
		sql.insertIntoCanBeat("Water", "Fire");
		sql.insertIntoCanBeat("Water", "Ground");
		sql.insertIntoCanBeat("Water", "Rock");
		
		sql.insertIntoCanBeat("Electric", "Water");
		sql.insertIntoCanBeat("Electric", "Flying");
		
		sql.insertIntoCanBeat("Grass", "Water");
		sql.insertIntoCanBeat("Grass", "Ground");
		sql.insertIntoCanBeat("Grass", "Rock");
		
		sql.insertIntoCanBeat("Ice", "Grass");
		sql.insertIntoCanBeat("Ice", "Ground");
		sql.insertIntoCanBeat("Ice", "Flying");
		sql.insertIntoCanBeat("Ice", "Dragon");
		
		sql.insertIntoCanBeat("Fighting", "Normal");
		sql.insertIntoCanBeat("Fighting", "Ice");
		sql.insertIntoCanBeat("Fighting", "Rock");
		sql.insertIntoCanBeat("Fighting", "Steel");
		sql.insertIntoCanBeat("Fighting", "Dark");
		
		sql.insertIntoCanBeat("Poison", "Grass");
		sql.insertIntoCanBeat("Poison", "Fairy");
		
		sql.insertIntoCanBeat("Ground", "Fire");
		sql.insertIntoCanBeat("Ground", "Electric");
		sql.insertIntoCanBeat("Ground", "Poison");
		sql.insertIntoCanBeat("Ground", "Rock");
		sql.insertIntoCanBeat("Ground", "Steel");
		
		sql.insertIntoCanBeat("Flying", "Grass");
		sql.insertIntoCanBeat("Flying", "Fighting");
		sql.insertIntoCanBeat("Flying", "Bug");
		
		sql.insertIntoCanBeat("Psychic", "Fighting");
		sql.insertIntoCanBeat("Psychic", "Poison");
		
		sql.insertIntoCanBeat("Bug", "Grass");
		sql.insertIntoCanBeat("Bug", "Psychic");
		sql.insertIntoCanBeat("Bug", "Dark");
		
		sql.insertIntoCanBeat("Rock", "Fire");
		sql.insertIntoCanBeat("Rock", "Ice");
		sql.insertIntoCanBeat("Rock", "Flying");
		sql.insertIntoCanBeat("Rock", "Bug");
		
		sql.insertIntoCanBeat("Ghost", "Psychic");
		sql.insertIntoCanBeat("Ghost", "Ghost");
		
		sql.insertIntoCanBeat("Dragon", "Dragon");
		
		sql.insertIntoCanBeat("Dark", "Psychic");
		sql.insertIntoCanBeat("Dark", "Ghost");
		
		sql.insertIntoCanBeat("Steel", "Grass");
		sql.insertIntoCanBeat("Steel", "Bug");
		sql.insertIntoCanBeat("Steel", "Fairy");
		
		sql.insertIntoCanBeat("Fairy", "Fighting");
		sql.insertIntoCanBeat("Fairy", "Dragon");
		sql.insertIntoCanBeat("Fairy", "Dark");
		
		
		sql.insertIntoIsImmune("Normal","Ghost");
		sql.insertIntoIsImmune("Electric","Ground");
		sql.insertIntoIsImmune("Fighting","Ghost");
		sql.insertIntoIsImmune("Poison","Steel");
		sql.insertIntoIsImmune("Ground","Flying");
		sql.insertIntoIsImmune("Psychic","Dark");
		sql.insertIntoIsImmune("Ghost","Normal");
		sql.insertIntoIsImmune("Dragon","Fairy");
		
		for(int i = 0; i< 151; i++){
			String name = pokedex.get(i).getName();
			PokemonType type_sql = pokedex.get(i).getType1();
			PokemonType type_sql2 = pokedex.get(i).getType2();
			String location = pokedex.get(i).getLocation();
			String image = pokedex.get(i).getImage();
			String info = pokedex.get(i).getInfo();
			int hp_sql = pokedex.get(i).getHp();
			int attack_sql = pokedex.get(i).getAttack();
			int defense_sql = pokedex.get(i).getDefense();
			int special_attack_sql = pokedex.get(i).getSpecialAttack();
			int special_defense_sql = pokedex.get(i).getSpecialDefense();
			int speed_sql = pokedex.get(i).getSpeed();
			String region = pokedex.get(i).getRegion();
			
			sql.insertIntoPokemonInfo(name, hp_sql, attack_sql, defense_sql, special_attack_sql, special_defense_sql, speed_sql);
			sql.insertIntoPokemonType(name, type_sql, type_sql2);
			//sql.insertIntoPokemon(name, region, info, hp_sql, attack_sql, defense_sql);
			//sql.insertIntoPokemonImg(name, image);
		}
		
	}

	public static String toUpperFirstLetter(String name){		
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}
	
	public static PokemonType returnType(String type){
		if(type.equals("Normal"))
			return PokemonType.Normal;
		else if(type.equals("Fire"))
			return PokemonType.Fire;
		else if(type.equals("Water"))
			return PokemonType.Water;
		else if(type.equals("Electric"))
			return PokemonType.Electric;
		else if(type.equals("Grass"))
			return PokemonType.Grass;
		else if(type.equals("Ice"))
			return PokemonType.Ice;
		else if(type.equals("Fighting"))
			return PokemonType.Fighting;
		else if(type.equals("Poison"))
			return PokemonType.Poison;
		else if(type.equals("Ground"))
			return PokemonType.Ground;
		else if(type.equals("Flying"))
			return PokemonType.Flying;
		else if(type.equals("Psychic"))
			return PokemonType.Psychic;
		else if(type.equals("Bug"))
			return PokemonType.Bug;
		else if(type.equals("Rock"))
			return PokemonType.Rock;
		else if(type.equals("Ghost"))
			return PokemonType.Ghost;
		else if(type.equals("Dragon"))
			return PokemonType.Dragon;
		else if(type.equals("Dark"))
			return PokemonType.Dark;
		else if(type.equals("Steel"))
			return PokemonType.Steel;
		else if(type.equals("Fairy"))
			return PokemonType.Fairy;
		else return PokemonType.Normal;
	}
}
















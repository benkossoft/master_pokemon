package PokemonDB;

import java.util.ArrayList;

public class Pokemon {
	
	private String name = null;
	private String id = null;
	private PokemonType type1 = null;
	private PokemonType type2 = null;
	private String info = null;
	private String location = null;
	private String image = null;
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	private int hp;
	private int attack;
	private int defense;
	private int specialAttack;
	private int specialDefense;
	private int speed;
	
	public int getSpecialAttack() {
		return specialAttack;
	}

	public void setSpecialAttack(int specialAttack) {
		this.specialAttack = specialAttack;
	}

	public int getSpecialDefense() {
		return specialDefense;
	}

	public void setSpecialDefense(int specialDefense) {
		this.specialDefense = specialDefense;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	private String region = null;
	
	public Pokemon(String name, String id, PokemonType type1, PokemonType type2, int hp, int attack, int defense,
			int specialAttack, int specialDefense, int speed,
			String website_image){
		if(id.equals("029")){
			this.name = name.substring(0, name.length()-1) + "-m";
		}
		else if(id.equals("032")){
			this.name = name.substring(0, name.length()-1) + "-f";
		}
		else this.name = name;
		
		this.id = id;
		this.type1 = type1;
		this.type2 = type2;
		this.hp = hp;
		this.attack = attack;
		this.defense = defense;
		this.specialAttack = specialAttack;
		this.specialDefense = specialDefense;
		this.speed = speed;
		this.image = website_image;
		
		int id_int = Integer.parseInt(id);
		if(id_int <= 151) region = "Kanto";
		else if(id_int <= 251) region = "Johto";
		else if(id_int <= 386) region = "Hoenn";
		else if(id_int <= 493) region = "Sinnoh";
		else if(id_int <= 649) region = "Unova";
		else if(id_int <= 721) region = "Kalos";
	}

	public PokemonType getType1(){
		return type1;
	}
	
	public PokemonType getType2(){
		return type2;
	}
	
	public ArrayList<PokemonType> getTypes(){
		ArrayList<PokemonType> types = new ArrayList<PokemonType>();
		if(type1 != null)
			types.add(type1);
		if(type2 != null)
			types.add(type2);
		return types;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		//return String.format("#" + "%03d", id);
		return id;
	}
	
	public boolean isEqual(String name){
		return this.name.equals(name);
	}

	@Override
	public String toString() {
		return "Pokemon" + id + "-[name=" + name + ", type1=" + type1 + ", type2=" + type2 + ", hp=" + hp + ", attack=" + attack
				+ ", defense=" + defense + ", specialAttack=" + specialAttack + ", specialDefense=" + specialDefense
				+ ", speed=" + speed + "]";
	}

	
	
	
	
	
	
}
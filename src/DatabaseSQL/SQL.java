package DatabaseSQL;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SQL {

	private java.sql.Connection connection = null;
	private PreparedStatement preparedStatement = null;
	
	final private String host = "localhost";
	final private String user = "root";
	final private String password = "";
	private String database = null;
	
	public void connectToDB(String database){
		this.database = database;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://"+host+"/"+this.database,user,password);			
			System.out.println("Connected to DB");		
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void insertIntoTypes(String type){
		
		String sql = "insert into types (name) values (?)";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, type);
			preparedStatement.executeUpdate();
			System.out.println(type + " is added");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void insertIntoRegion(String name, int level_first, int level_second){
		
		String sql = "insert into region (name, level_first, level_second) values (?,?,?)";
		
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, name);
			preparedStatement.setInt(2, level_first);
			preparedStatement.setInt(3, level_second);
			preparedStatement.executeUpdate();
			System.out.println(name +" region is added with "+ level_first +"-" + level_second);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void insertIntoCanBeat(String type1, String type2){
		
		String sql = "insert into canbeat (type1, type2) values (?,?)";
		
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, type1);
			preparedStatement.setString(2, type2);
			preparedStatement.executeUpdate();
			System.out.println("("+type1+","+type2+") beating relation is added!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void insertIntoIsImmune(String type1, String type2){
		
		String sql = "insert into isimmune (type1, type2) values (?,?)";
		
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, type1);
			preparedStatement.setString(2, type2);
			preparedStatement.executeUpdate();
			System.out.println("("+type1+","+type2+") immune relation is added!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void insertIntoPokemonInfo(String name, int hp, int attack, int defence,
			int special_attack, int special_defense, int speed){
		
		String sql = "insert into pokemoninfo (name,	"
				+ "hp, attack, defence, special_attack,"
				+ "special_defense, speed, legendary) values (?,?,?,?,?,?,?,?)";
		
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, name);
			preparedStatement.setInt(2, hp);
			preparedStatement.setInt(3, attack);
			preparedStatement.setInt(4, defence);
			preparedStatement.setInt(5, special_attack);
			preparedStatement.setInt(6, special_defense);
			preparedStatement.setInt(7, speed);
			preparedStatement.setBoolean(8, false);
			preparedStatement.executeUpdate();
			System.out.println("Pokemon " + name+ " is added!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void insertIntoPokemon(String name, String region, String info, int hp, int attack, int defense){
		
		String sql = "insert into pok�mon (pid,	"
				+ "pName, region, HP, attack,"
				+ "defense, info) values (NULL,?,?,?,?,?,?)";
		
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, region);
			preparedStatement.setInt(3, hp);
			preparedStatement.setInt(4, attack);
			preparedStatement.setInt(5, defense);
			preparedStatement.setString(6, info);
			preparedStatement.executeUpdate();
			System.out.println("Pokemon " + name+ " is added!");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void insertIntoPokemonType(String name, PokemonDB.PokemonType type1,
			PokemonDB.PokemonType type2){
		
		String sql = "insert into istype (pokemon_name,"
				+ "type1, type2) values (?,?,?)";
		
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, type1.toString());
			if(type2 != null){
				preparedStatement.setString(3, type2.toString());
			}
			else{
				preparedStatement.setString(3, null);
			}
			preparedStatement.executeUpdate();
			System.out.println("PokemonType with " + name+ " is added!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void insertIntoPokemonImg(String name, String image){
		
		String sql = "insert into pok�monimg (pName, image) values (?,?)";
		
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, image);
			preparedStatement.executeUpdate();
			System.out.println("PokemonImg with " + name+ " is added!");	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	
	
}
